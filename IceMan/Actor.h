#ifndef ACTOR_H_
#define ACTOR_H_

#include "GraphObject.h"

class StudentWorld;

// Students:  Add code to this file, Actor.cpp, StudentWorld.h, and StudentWorld.cpp

//hello hihi
class Actor : public GraphObject {

public:
	// Constructor
	Actor(int ID, int x, int y, Direction dir, double size, unsigned int depth)
		: GraphObject(ID, x, y, dir, size, depth) {}
	virtual void doSomething() = 0;
	// Destructor
	~Actor() {}
};


class Ice : public Actor {

public:
	// Constructor
	Ice(int x, int y) : Actor(IID_ICE, x, y, right, .25, 3) {}
	virtual void doSomething() override { /* Do Ice stuff */ }
	// Destructor
///	~Ice() { std::cout << "Ice Broken" << std::endl; }
};


class Iceman : public Actor {

private:
	StudentWorld* m_sWorld;
	int m_hp;
	int m_water;
	int m_sonar;
	int m_gold;

public:
	// Constructor
	Iceman(StudentWorld* swp) : Actor(IID_PLAYER, 30, 60, right, 1.0, 0), m_hp(10), 
		m_sWorld(swp), m_water(5), m_sonar(1), m_gold(0) {}
	virtual void doSomething() override;
	// Destructor
///	~Iceman() { std::cout << "Iceman Died" << std::endl; }
};


#endif // ACTOR_H_