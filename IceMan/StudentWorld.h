#ifndef STUDENTWORLD_H_
#define STUDENTWORLD_H_

#include "GameWorld.h"
#include "GameConstants.h"
#include "Actor.h"
#include <string>
#include <memory>
#include <vector>

// Students:  Add code to this file, StudentWorld.cpp, Actor.h, and Actor.cpp

class StudentWorld : public GameWorld
{
public:
	StudentWorld(std::string assetDir)
		: GameWorld(assetDir)
	{
	}

	virtual int init()
	{
		m_iceman = std::make_unique<Iceman>(this);
		m_iceman->setVisible(true);
		for (int i = 0; i < VIEW_WIDTH; i++) {
			std::vector<std::shared_ptr<Ice>> col;
			for (int k = 0; k < VIEW_HEIGHT; k++) {
				col.push_back(std::shared_ptr<Ice>(new Ice(i, k)));
				if (k < VIEW_HEIGHT - 4) {
					for (const auto& inner : col)
						inner->setVisible(true);
				}
			}

			m_ice.push_back(std::move(col));
		}
		return GWSTATUS_CONTINUE_GAME;
	}

	virtual int move()
	{
		// This code is here merely to allow the game to build, run, and terminate after you hit enter a few times.
		// Notice that the return value GWSTATUS_PLAYER_DIED will cause our framework to end the current level.
		m_iceman->doSomething();
		return GWSTATUS_CONTINUE_GAME;

		decLives();
		return GWSTATUS_PLAYER_DIED;
	}

	virtual void cleanUp()
	{
	}

	std::vector<std::vector<std::shared_ptr<Ice>>> getIce() { return m_ice; }

private:
	std::vector<std::vector<std::shared_ptr<Ice>>> m_ice;
	std::unique_ptr<Iceman> m_iceman;
};

#endif // STUDENTWORLD_H_
